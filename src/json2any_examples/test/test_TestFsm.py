from typing import Optional
from unittest import TestCase

from json2any_examples.fsm import StateId, EvtId, ACtx
from json2any_examples.fsm.Event import Event
from json2any_examples.fsm.AState import AState
from json2any_examples.fsm.Fsm import Fsm
from json2any_examples.fsm.InitialState import InitialState


class TestCtx(ACtx):

    def __init__(self):
        self.counter = 0
        self.s1_enter_counter = 0
        self.s1_exit_counter = 0


class TestEventId(EvtId):
    S1S2 = 'S1S2',
    S2S1 = 'S2S1'
    INC = 'INC'
    INC_EXT = 'INC_EXT'


class TestStateId(StateId):
    INITIAL = 'INITIAL'
    S1 = 'S1',
    S2 = 'S2'
    FINAL = 'FINAL'


class TestStateS1(AState):

    def on_entry(self, ctx: ACtx):
        ctx.s1_enter_counter += 1

    def on_exit(self, ctx: ACtx):
        ctx.s1_exit_counter += 1


class TestStateS2(AState):
    pass


def action_inc(ctx: TestCtx, *args, **kwargs) -> bool:
    ctx.counter += 1
    return True


action_inc.description = 'increment counter'


def guard_odd(ctx: TestCtx, *args, **kwargs) -> bool:
    if ctx.counter % 2 == 0:
        return True
    return False


guard_odd.description = 'counter is odd'


class TestFsm(TestCase):

    def __init__(self, methodName="runTest"):
        super().__init__(methodName)
        self.fsm: Optional[Fsm] = None
        self.initial: Optional[TestStateS1] = None
        self.s1: Optional[TestStateS1] = None
        self.s2: Optional[TestStateS2] = None

    def setUp(self):
        super().setUp()
        self.fsm = Fsm(TestStateId, TestEventId, TestCtx())
        self.initial = InitialState(TestStateId.INITIAL)
        self.s1 = TestStateS1(TestStateId.S1)
        self.s2 = TestStateS2(TestStateId.S2)

    def registerStates(self):
        self.fsm.register_state(self.s1)
        self.fsm.register_state(self.s2)
        self.fsm.register_state(self.initial)

    def default_transitions(self):
        self.initial.register_transition(None, is_internal=False, dst_state_id=TestStateId.S1)
        self.s1.register_transition(TestEventId.S1S2, is_internal=False, dst_state_id=TestStateId.S2)
        self.s1.register_transition(TestEventId.INC, is_internal=True, action=action_inc)
        self.s1.register_transition(TestEventId.INC_EXT, is_internal=False, action=action_inc,
                                    dst_state_id=TestStateId.S1)

        self.s2.register_transition(TestEventId.S2S1, is_internal=False, dst_state_id=TestStateId.S1)
        self.s2.register_transition(TestEventId.INC, is_internal=True, guard=guard_odd, action=action_inc)

    def test_ctor(self):
        self.assertEqual(self.s1.id, TestStateId.S1)
        self.assertEqual(self.s2.id, TestStateId.S2)
        self.assertEqual(0, len(self.s1.transitions))

    def test_register_transition(self):
        self.registerStates()

        self.initial.register_transition(None, is_internal=False, dst_state_id=TestStateId.S1)
        self.s1.register_transition(TestEventId.S1S2, is_internal=False, dst_state_id=TestStateId.S2,
                                    description='S1toS2')
        self.s1.register_transition(TestEventId.S2S1, is_internal=False, dst_state_id=TestStateId.S1,
                                    description='S2toS1')

        self.assertEqual(2, len(self.s1.transitions))

        self.assertEqual(TestStateId.INITIAL, self.fsm.current_state_id)

        self.fsm.process_event(None)
        self.assertEqual(TestStateId.S1, self.fsm.current_state_id)

    def test_transitions(self):
        self.registerStates()

        self.default_transitions()
        self.assertEqual(TestStateId.INITIAL, self.fsm.current_state_id)
        self.assertEqual(0, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(0, self.fsm.ctx.s1_exit_counter)
        self.assertEqual(0, self.fsm.ctx.counter)

        self.fsm.process_event(None)
        self.assertEqual(TestStateId.S1, self.fsm.current_state_id)
        self.assertEqual(1, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(0, self.fsm.ctx.s1_exit_counter)
        self.assertEqual(0, self.fsm.ctx.counter)

        # couple of increments
        self.fsm.process_event(Event(TestEventId.INC))
        self.assertEqual(1, self.fsm.ctx.counter)
        self.fsm.process_event(Event(TestEventId.INC))
        self.assertEqual(2, self.fsm.ctx.counter)
        self.assertEqual(1, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(0, self.fsm.ctx.s1_exit_counter)

        # invalid event for state
        self.fsm.process_event(Event(TestEventId.S2S1))
        self.assertEqual(TestStateId.S1, self.fsm.current_state_id)
        self.assertEqual(2, self.fsm.ctx.counter)
        self.assertEqual(1, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(0, self.fsm.ctx.s1_exit_counter)

        # go to S2
        self.fsm.process_event(Event(TestEventId.S1S2))
        self.assertEqual(TestStateId.S2, self.fsm.current_state_id)
        self.assertEqual(2, self.fsm.ctx.counter)
        self.assertEqual(1, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(1, self.fsm.ctx.s1_exit_counter)

        # with odd counter the increments will not work - guard
        self.fsm.process_event(Event(TestEventId.INC))
        self.fsm.process_event(Event(TestEventId.INC))
        self.assertEqual(3, self.fsm.ctx.counter)
        self.assertEqual(1, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(1, self.fsm.ctx.s1_exit_counter)

        # back to s1 and increment counter and back to S2
        self.fsm.process_event(Event(TestEventId.S2S1))
        self.fsm.process_event(Event(TestEventId.INC))
        self.fsm.process_event(Event(TestEventId.S1S2))
        self.assertEqual(2, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(2, self.fsm.ctx.s1_exit_counter)

        # now it should work again
        self.fsm.process_event(Event(TestEventId.INC))
        self.fsm.process_event(Event(TestEventId.INC))
        self.assertEqual(5, self.fsm.ctx.counter)
        self.assertEqual(2, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(2, self.fsm.ctx.s1_exit_counter)

        # back to s1 to test self-external
        self.fsm.process_event(Event(TestEventId.S2S1))
        self.assertEqual(5, self.fsm.ctx.counter)
        self.assertEqual(3, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(2, self.fsm.ctx.s1_exit_counter)

        self.fsm.process_event(Event(TestEventId.INC_EXT))
        self.assertEqual(6, self.fsm.ctx.counter)
        self.assertEqual(4, self.fsm.ctx.s1_enter_counter)
        self.assertEqual(3, self.fsm.ctx.s1_exit_counter)
