from unittest import TestCase

from json2any_examples.fsm import StateId


class InvalidTestStateId(StateId):
    S1 = 'S1'
    S2 = 'S2'
    S3 = 'S3'


class Invalid2TestStateId(StateId):
    S1 = 'S1'
    S2 = 'S2'
    INITIAL = 'INITIAL'
    S3 = 'S3'


class ValidTestStateId(StateId):
    INITIAL = 'INITIAL'
    S1 = 'S1'
    S2 = 'S2'
    S3 = 'S3'


class TestFsmId(TestCase):
    pass

    def test_ctor(self):
        v = InvalidTestStateId.S1
        self.assertEqual(v.value, 1)
        self.assertEqual(v.name, "S1")

    def test_keys(self):
        self.assertTrue(InvalidTestStateId.S1 in InvalidTestStateId)

    def test_state_ids_no_initial(self):
        with self.assertRaises(Exception) as cm:
            StateId.validate_enum(InvalidTestStateId)
        self.assertTrue(isinstance(cm.exception, ValueError))

    def test_state_ids_initial_not_first(self):
        with self.assertRaises(Exception) as cm:
            StateId.validate_enum(Invalid2TestStateId)
        self.assertTrue(isinstance(cm.exception, ValueError))

    def test_state_ids_valid(self):
        StateId.validate_enum(ValidTestStateId)
        self.assertTrue(True)
