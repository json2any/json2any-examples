from abc import ABC
from typing import Optional, Dict, List

from json2any_examples.fsm.AState import StateId, AState


class HasChildren(ABC):

    def __init__(self, id: StateId, dflt_child_id: Optional['StateId'] = None,
                 parent_state: Optional['AState'] = None):
        super().__init__()

        self.id = id
        self.__dflt_child_id = dflt_child_id
        self.__parent_state: Optional[AState] = parent_state
        self.__children: Dict[StateId, AState] = {}

    @property
    def parent(self) -> Optional['AState']:
        return self.__parent_state

    @property
    def dflt_child_id(self) -> StateId:
        return self.__dflt_child_id

    def add_child(self, state: 'AState'):
        if state.id in self.__children:
            raise Exception('Child state already registered')
        self.__children[state.id] = state

    def find_child(self, state_id: StateId, path: List[StateId] = ()):
        for child_id, child in self.__children.items():
            if state_id == child_id:
                path.append(child_id)
            else:
                child.fi
                break
        if len(path) > 0:
            path.append(self.id)
