from abc import ABC
from typing import Optional

from json2any_examples.fsm import Action
from json2any_examples.fsm.AState import EvtId, StateId, Guard


class Transition(ABC):
    def __init__(self, evt_id: Optional[EvtId], is_internal: bool, dst_state_id: Optional[StateId],
                 guard: Optional[Guard],
                 action: Optional[Action], description: str = None):
        super().__init__()

        if not is_internal and dst_state_id is None:
            raise Exception('Invalid transition - external transaction must have a destination state')

        self.evt_id = evt_id
        self.is_internal = is_internal
        self.dst_state_id = dst_state_id
        self.guard = guard
        self.action = action
        if description is None:
            description = f'{evt_id}'
            if guard is not None:
                description += f'[{guard}]'
            if action is not None:
                description += f'/{action}'
        self.description = description

    def __str__(self):
        return self.description

    def __repr__(self):
        return self.__str__()
