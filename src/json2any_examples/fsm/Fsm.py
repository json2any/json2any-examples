import os
from abc import ABC
from logging import getLogger
from queue import Queue
from typing import Type, Dict, Optional, List

from json2any_examples.fsm import ACtx, Guard, OnExitException, StateId, OnEnterException, OnActionException, \
    FsmException
from json2any_examples.fsm.AState import AState
from json2any_examples.fsm.Event import Event
from json2any_examples.fsm.Transition import Transition


class Fsm(ABC):
    def __init__(self, state_id_cls: Type[StateId], event_id_cls: Type, ctx: ACtx, max_event_queue_size=0,
                 fsm_name='FSM'):
        super().__init__()
        self.logger = getLogger(fsm_name)

        StateId.validate_enum(state_id_cls)
        self.ctx = ctx
        self.state_id_cls = state_id_cls
        self.event_id_cls = event_id_cls
        self.fsm_name = fsm_name

        self.__states: Dict[StateId, AState] = {}
        self.__current_state: Optional[AState] = None
        self.__evt_queue: Queue[Event] = Queue(maxsize=max_event_queue_size)

    def validate(self):
        # if StateId.INITIAL not in self.__states:
        #     raise FsmException(f'Initial state not registered')
        pass

    @property
    def current_state_id(self) -> StateId:
        if self.__current_state is None:
            raise Exception(f'INITIAL state yet to be registered')
        return self.__current_state.id

    def register_state(self, state: 'AState'):
        if state.id in self.__states:
            raise ValueError(f'State {state.id} already exists')

        state.ctx = self.ctx
        self.__states[state.id] = state
        state.fsm = self

        if state.id == self.state_id_cls.INITIAL:
            self.__current_state = state

    def process_event(self, evt: Optional[Event], *args, **kwargs):

        if self.__current_state is None:
            raise FsmException(f'Default state yet to be registered')

        valid_transitions: List[Transition] = []
        for (key, tr) in self.__current_state.transitions.items():
            (evt_id, guard) = key
            guard: Guard

            if evt is None:
                if evt_id is not None:
                    continue
            else:  # evt is not None
                if evt_id != evt.id:
                    continue

            if guard is not None:
                guard_value = guard(self.ctx, *args, **kwargs)
                if not guard_value:
                    continue
            valid_transitions.append(tr)

        if len(valid_transitions) > 1:
            trstr = [str(t) for t in valid_transitions]
            raise FsmException(
                f'Multiple valid transitions in state:{self.current_state_id} for event "{evt}": {os.linesep.join(trstr)}')

        if len(valid_transitions) == 0:
            return
        transition: Transition = valid_transitions[0]

        if not transition.is_internal:
            try:
                self.__current_state.on_exit(self.ctx)
            except Exception as ex:
                raise OnExitException(self.current_state_id, evt.id) from ex

        if transition.action:
            try:
                transition.action(self.ctx, *args, **kwargs)
            except Exception as ex:
                raise OnActionException(self.current_state_id, evt.id) from ex

        if not transition.is_internal:
            if transition.dst_state_id is not None:
                dst_state = self.__states[transition.dst_state_id]
            else:
                dst_state = self.__current_state

            try:
                dst_state.on_entry(self.ctx)
            except Exception as ex:
                raise OnEnterException(self.current_state_id, evt.id) from ex

            self.__current_state = dst_state
