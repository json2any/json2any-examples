from enum import Enum
from logging import getLogger, Logger
from typing import Protocol, Type

logger: Logger = getLogger('fsm')


class __FsmId(Enum):

    def __new__(cls, *args, **kwds):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    @classmethod
    def keys(cls):
        return [k.value for k in list(cls)]

    @classmethod
    def names(cls):
        return [k.name for k in list(cls)]

    def __repr__(self):
        return self.name


class EvtId(__FsmId):
    pass


class StateId(__FsmId):
    @classmethod
    def validate_enum(cls, state_id_enum: Type['StateId']):
        elems = list(state_id_enum)

        n_entries = len(elems)

        if n_entries < 2:
            raise ValueError(f'"{state_id_enum.__name__}" enum must have at least two entries INITIAL and FINAL')
        initial = elems[0]
        if initial.name != 'INITIAL':
            raise ValueError(f'"{state_id_enum.__name__}" enum must have INITIAL as first entry')
        if initial.value != 1:
            raise ValueError(f'"{state_id_enum.__name__}" enum must have INITIAL as first entry')


class ACtx:
    pass


class Action(Protocol):
    def __call__(self, ctx: ACtx, *args, **kwargs) -> bool: ...


class Guard(Protocol):
    def __call__(self, ctx: ACtx, *args, **kwargs) -> bool: ...


class OnExitException(Exception):

    def __init__(self, state_id: StateId, evt_id: EvtId, *args):
        super().__init__(state_id, evt_id, *args)


class OnEnterException(Exception):
    def __init__(self, state_id: StateId, evt_id: EvtId, *args):
        super().__init__(state_id, evt_id, *args)


class OnActionException(Exception):
    def __init__(self, state_id: StateId, evt_id: EvtId, *args):
        super().__init__(state_id, evt_id, *args)


class FsmException(Exception):
    def __init__(self, *args):
        super().__init__(*args)
