from abc import ABC
from typing import Optional, Dict, Tuple

from json2any_examples.fsm import StateId, EvtId, Action, Guard, ACtx
from json2any_examples.fsm.Transition import Transition


class AState(ABC):

    def __init__(self, id_: StateId):
        super().__init__()

        self.__id = id_
        self.__ctx: Optional[ACtx] = None
        self.__transitions: Dict[Tuple[EvtId, Guard], Transition] = {}

    @property
    def id(self):
        return self.__id

    @property
    def ctx(self) -> ACtx:
        return self.__ctx

    @ctx.setter
    def ctx(self, value: ACtx):
        if self.__ctx is not None:
            raise Exception('Context already set')
        self.__ctx = value

    @property
    def transitions(self):
        return self.__transitions

    def register_transition(self, evt_id: Optional[EvtId], is_internal: bool, dst_state_id: Optional[StateId] = None,
                            guard: Optional[Guard] = None, action: Optional[Action] = None, description: str = None):

        key = (evt_id, guard)
        if key in self.__transitions:
            raise Exception(f'Duplicate transition: "({evt_id},{guard})"')

        self.__transitions[key] = Transition(evt_id, is_internal, dst_state_id=dst_state_id, guard=guard, action=action,
                                             description=description)

    def on_entry(self, ctx: ACtx):
        pass

    def on_exit(self, ctx: ACtx):
        pass

    def __str__(self):
        return self.id.name
