from abc import ABC

from json2any_examples.fsm.AState import EvtId


class Event(ABC):

    def __init__(self, id_: EvtId):
        super().__init__()
        self.__id: EvtId = id_

    @property
    def id(self) -> EvtId:
        return self.__id

    def __str__(self):
        return self.id.name
